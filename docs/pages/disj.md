---
layout: default
---

# ChatGPT and disjunction-under-negation

* * *

## Disjunction and negation: intro

English language has sentences that contain both logical disjunction (<code>or</code>) and logical negation (<code>not</code>) in them. For example:

> My friend did _not_ eat apples _or_ oranges.

The structure of this sentence can be parsed as

```tex
[My friend did not eat [apples or oranges.]]
```

In this case, from a syntactic point of view, _not_ is c-commanding _or_ since it precedes it.[^1]

[^1]: Slightly reverse-engineering LCA here. <em>Asymmetric c-command maps onto precedence relations</em>; Kayne's Linear Correspondence axiom. See Kayne, R. S. (1994). The antisymmetry of syntax (Vol. 25). MIT press.

Other languages have similar constructions too, for example:

|   | Language         | Sentence    |
|:--|:--------------------------------|:------------------|
| 1 | Ukrainian | Мій друг не їв яблука чи апельсини. |
| 2 | German | Mein Freund hat Äpfel oder Orangen night gegessen. |
| 3 | Mandarin Chinese | 我的朋友不吃蘋果或橘子。|


From a purely formal point of view, there shouldn't be ambiguity about how to interpret these: the laws of logic[^2] clearly specify that *not (A or B)* is interpreted as *not A and not B*. Therefore, the English example above (for a native speaker of English), should be synonymous with

[^2]: Specifically, De Morgan's law. See [here](https://en.wikipedia.org/wiki/De_Morgan%27s_laws) for further reference.

> My friend did not eat apples and my friend did not eat oranges.

Or alternatively,

> My friend ate neither apples nor oranges.

Indeed, for language like English that is the case. However, other languages refuse to obey such constraints. For example, Ukrainian seems to have two conjunctions, *chy* and *abo* -- one of them giving the *neither* interpretation, the other one -- *not both*. Some languages seem to favor exclusively *not both*, like French[^3].  

[^3]: See Nicolae, A. C. (2017). Deriving the positive polarity behavior of plain disjunction. Semantics and Pragmatics, 10, 5-1. (et seq.)

Not enough work has been done to explain why these phenomena appear, but what we are sure of is that there's cross-linguistic variation in this not-over-or phenomenon. This is further amplified by the fact that some languages have two or three disjunctive connectors.

## ChatGPT

It is interesting, however, to see whether ChatGPT is able to interpret such sentences correctly (as a native speaker). I am not sure I would expect nativelike behavior here since the stimuli are so rare (check COCA). Like parasitic gaps[^4], not-over-or seems to occur rarely in natural speech and so ChatGPT shouldn't have much ground to derive the interpretation from.    

[^4]: Cf. e.g., Phillips, C. (2013). On the nature of island constraints II: Language learning and innateness. Experimental syntax and island effects, 132-157.


## Experimental paradigm

To check the authenticity of ChatGPT's judgments, we'll use an experimental paradigm roughly similar to the one implemented by Gruter and colleagues[^5] back in 2010. Using human experimental paradigm is not novel.[^6]

[^5]: Grüter, T., Lieberman, M., & Gualmini, A. (2010). Acquiring the scope of disjunction and negation in L2: A bidirectional study of learners of Japanese and English. Language Acquisition, 17(3), 127-154.
[^6]: Here's a sample paper suggested to be by [Rob](https://faculty.wcas.northwestern.edu/robvoigt/) which does this: Mahowald, K. (2023). A Discerning Several Thousand Judgments: GPT-3 Rates the Article+ Adjective+ Numeral+ Noun Construction. arXiv preprint arXiv:2301.12564.

> > The implementation section including code will follow immediately below.

What actually happens is we give ChatGPT a situation, e.g.:

> Joseph, Kevin, and Knut were reading books. The books were  "Macbeth" and "The Cat in the Hat".  For every book read, they were given a flower. If they read one book, they received one flower. If they read two books, they received two flowers; and if they red three books, they received thee flowers.

Then, a human (or ChatGPT) is asked to evaluate a sentence like this (is it true or false == possible or not?):

> Joseph did not read "Macbeth" or "The Cat in the Hat". He received no flowers.

In the English-speaking world, Joseph did not read anything and has no flowers. In a French-speaking world, however, Joseph should have received one flower. We can ask ChatGPT about this.

However, we are also interested in cross-linguistic results. As such, we will ask ChatGPT the following:

* Is it able to correctly interpret English sentences or does ChatGPT do it randomly?
* Is ChatGPT able to to correctly interpret stimuli from Ukrainian?
* Does ChatGPT's ability to interpret the sentences depend on the simplicity of the stimuli (i.e., would including an RC throw it off)?

## Technicalities

This section describes how the results (discussed in the following section) were obtained. First of all,

```python
pip3 install openai
import openai
```

ChatGPT (aka <code>gpt-3.5-turbo</code>) requires a token, so one needs to set up a payment method and obtain something to the effect of

```python
openai.api_key = "Get your own key!"
```

We can run a sample interaction with the GPT[^7]:

[^7]: The stimulus I ran included a typo 'speel' instead of 'sleep', but since it doesn't really matter I did not re-run it.

```python
content = "Is 'Colorless green ideas speel furiously' a grammatical sentence of English? Is 'He didn't eat apples or pies.' grammatical as well?"
text = openai.ChatCompletion.create(
  model="gpt-3.5-turbo",
  messages=[{"role": "user", "content": content}]
)
```

For which we get an output with a fair bit of information (via <code>print(text)</code>):

```python
{
  "choices": [
    {
      "finish_reason": "stop",
      "index": 0,
      "message": {
        "content": "\n\nYes, \"Colorless green ideas speel furiously\" and \"He didn't eat apples or pies\" are grammatically correct sentences of English, although the first sentence is semantically nonsensical.",
        "role": "assistant"
      }
    }
  ],
  "created": 1678650118,
  "id": "chatcmpl-6tLrqnZYMy5h5Vga83Tdv8pN7op4J",
  "model": "gpt-3.5-turbo-0301",
  "object": "chat.completion",
  "usage": {
    "completion_tokens": 43,
    "prompt_tokens": 40,
    "total_tokens": 83
  }
}
```

The only crucial bit is the actual response at:

```python
what_we_want = text.choices[0].message.content
```

a.k.a the string

> Yes, "Colorless green ideas speel furiously" and "He didn't eat apples or pies" are grammatically correct sentences of English, although the first sentence is semantically nonsensical.

The study mentioned above (Mahowald) used training of the model with several sample questions. E.g., the prompt told the model that sentences can be acceptable and unacceptable and gave examples. Then, the model determined acceptability on its own. There's no need for this in our setup since we give the scenario outright. It also simplifies the computational task by not keeping the message history.

As such, we just prepare the stimuli, input them, extract and save the results and then run the convenient statistics on them to report.


## Input to ChatGPT: English

So, the question posed is whether ChatGPT is able to reliably interpret not-over-or sentences in English. There will be three sets of stimuli:

### Set One: Twenty students will be reading books, e.g.,:

#### General form of stimuli:
  ```
  [Name] did not read [book1] and [book2]; they received [n] flower(s).
  ```

#### The code to generate the stimuli

```python
def set_one(n):
    s = ""
    for _ in range(n):
        s = s + f"{random.choice(names)} did not read {random.choice(books)} or {random.choice(books)}. {random.choice(outcome)}\n"
    return s
```

  <details>
    <summary>List of the generated stimuli (n = 30) </summary>
<br>
1 Yi Ming did not read The Cat in the Hat or Clockwork Orange. They got no flowers.<br>
0 Taichi did not read Optimist's Daughter or 1984. They got one flower.<br>
0 Keith did not read The Faerie Queene or 1984. They got one flower.<br>
0 Tae In did not read Great Expectations or The Faerie Queene. They got one flower.<br>
1 Emma did not read The Rime of the Ancient Mariner or American Tragedy: Stoic. They got no flowers.<br>
1 Erin did not read Macbeth or Optimist's Daughter. They got no flowers.<br>
1 Suzanne did not read The Cat in the Hat or Hatter's Castle. They got no flowers.<br>
1 Yi Ming did not read 墨子 or Optimist's Daughter. They got no flowers.<br>
0 Rob did not read Macbeth or 墨子. They got one flower.<br>
0 Taishu did not read As I Lay Dying or Hatter's Castle. They got one flower.<br>
1 Miles did not read 1984 or The Rime of the Ancient Mariner. They got no flowers.<br>
0 Karen did not read The Faerie Queene or Oliver Twist. They got one flower.<br>
1 Masaya did not read Optimist's Daughter or Oliver Twist. They got no flowers.<br>
0 Keith did not read Atlas Shrugged or Hatter's Castle. They got one flower.<br>
0 Isaac did not read Hatter's Castle or Optimist's Daughter. They got one flower.<br>
0 Masaya did not read The Cat in the Hat or The Rime of the Ancient Mariner. They got one flower.<br>
1 Keith did not read Sing, Unburied, Sing or Oliver Twist. They got no flowers.<br>
0 Erin did not read Macbeth or 墨子. They got one flower.<br>
0 Isaac did not read As I Lay Dying or Great Expectations. They got one flower.<br>
0 Masaya did not read Clockwork Orange or American Tragedy: Stoic. They got one flower.<br>
0 Shi Huang did not read Bartleby the Scrivener or Great Expectations. They got one flower.<br>
0 Wu Di did not read Bartleby the Scrivener or 1984. They got one flower.<br>
0 Rob did not read Clockwork Orange or Hatter's Castle. They got one flower.<br>
1 Keith did not read Macbeth or Macbeth. They got no flowers.<br>
0 Shi Huang did not read American Tragedy: Stoic or Oliver Twist. They got one flower.<br>
1 Isaac did not read Great Expectations or The Cat in the Hat. They got no flowers.<br>
1 Taichi did not read Optimist's Daughter or Bartleby the Scrivener. They got no flowers.<br>
0 Rob did not read Hatter's Castle or Bartleby the Scrivener. They got one flower.<br>
1 Erin did not read 墨子 or Hatter's Castle. They got no flowers.<br>
1 Erin did not read Macbeth or The Faerie Queene. They got no flowers.<br>
<br>
  </details><br>

### Set Two: Same, but asking about the number of flowers:

#### General form of stimuli
  ```
  [Name] did not read [book1] or [book2].
  ```

#### The code to generate the stimuli

Generated as follows:

```python
def set_two(n):
    s = ""
    for _ in range(n):
        s = s + f"{random.choice(names)} did not read {random.choice(books)} or {random.choice(books)}.\n"
    return s
```

<details>
  <summary>List of sample stimuli (n = 30)</summary>
<br>
Joseph did not read Optimist's Daughter or Hatter's Castle.<br>
Derek did not read Optimist's Daughter or American Tragedy: Stoic.<br>
Derek did not read The Cat in the Hat or Titus Andronicus.<br>
Gregory did not read The Raven or Atlas Shrugged.<br>
Taichi did not read The Faerie Queene or Macbeth.<br>
Taishu did not read Oliver Twist or The Raven.<br>
Rob did not read Titus Andronicus or The Raven.<br>
Haeun did not read Bartleby the Scrivener or Hatter's Castle.<br>
Bai Qi did not read As I Lay Dying or American Tragedy: Stoic.<br>
Erin did not read Oliver Twist or Atlas Shrugged.<br>
Bai Qi did not read Titus Andronicus or As I Lay Dying.<br>
Rob did not read Hamnet or Canterbury Tales.<br>
Tae In did not read 墨子 or 1984.<br>
Miles did not read The Raven or Hamnet.<br>
Emma did not read Sing, Unburied, Sing or Hamnet.<br>
Shi Huang did not read The Faerie Queene or 墨子.<br>
Olivia did not read Titus Andronicus or Hatter's Castle.<br>
Wu Di did not read Hamnet or The Cat in the Hat.<br>
Gregory did not read Great Expectations or Atlas Shrugged.<br>
Davi did not read 墨子 or 墨子.<br>
Shi Huang did not read The Rime of the Ancient Mariner or American Tragedy: Stoic.<br>
Rob did not read Atlas Shrugged or Oliver Twist.<br>
Joseph did not read Atlas Shrugged or Atlas Shrugged.<br>
Olivia did not read Titus Andronicus or Hatter's Castle.<br>
Tae In did not read Bartleby the Scrivener or Macbeth.<br>
Tae In did not read Macbeth or The Rime of the Ancient Mariner.<br>
Emma did not read 墨子 or Optimist's Daughter.<br>
Wu Di did not read Hamnet or Optimist's Daughter.<br>
Emma did not read Atlas Shrugged or Hatter's Castle.<br>
Emma did not read Canterbury Tales or The Cat in the Hat.<br>
<br>
</details><br>

#### Set Three: Twenty children will be playing games and giving their opinions on those games (as RCs).  

##### General form of stimuli

  ```
  [Modifier], [Name] did not play [game [XP]] or [game [XP]]. They received [x] apple(s).
  ```

##### The code to generate the stimuli

Generated as follows:

```python
def set_three(n):
    s = ""
    for _ in range(n):
        s = s + f"{random.choice(modifiers)}, {random.choice(names)} did not play {random.choice(games)}, {random.choice(XPs)}, or {random.choice(games)}, {random.choice(XPs)}. {random.choice(apple-options)}\n"
    return s
```

<details>
  <summary>List of sample stimuli (n = 30)</summary>
<br>
0 Once upon a time, Suzanne did not play Poker, even though they wanted to do something else, or Checkers, which they enjoyed. They got one apple. <br>
1 Once upon a time, Olivia did not play Dominion, including murdering their own ghost in the cellar, or Tag, regardless of age. They got no apples. <br>
1 At some point in the fall, Emma did not play Monopoly, which they enjoyed, or Cards, which made them feel personally attacked over an issue of interpreting the rules since they have been playing this game for over ten years now. They got no apples. <br>
1 Having been buried, Joseph did not play Cards, which made them feel personally attacked over an issue of interpreting the rules since they have been playing this game for over ten years now, or Catan, which they though was a waste of their time. They got no apples. <br>
1 Once the sun rose, Miles did not play Blackjack, which made them feel personally attacked over an issue of interpreting the rules since they have been playing this game for over ten years now, or Scrabble, which they hated. They got no apples. <br>
1 Having been buried, Tae In did not play Backgammon, regardless of age, or Poker, even though they wanted to do something else. They got no apples. <br>
0 After they ate all the people, Taichi did not play Tag, regardless of age, or Go, while getting drunk. They got one apple. <br>
0 Having been buried, Erin did not play Scrabble, which they enjoyed, or Pictionary, even though they wanted to do something else. They got one apple. <br>
1 Once the sun rose, Derek did not play Go, while getting drunk, or Bridge, regardless of age. They got no apples. <br>
0 Once upon a time, Yi Ming did not play Go, which they enjoyed, or Scrabble, which they enjoyed. They got one apple. <br>
0 Having been buried, Erin did not play Uno, which they enjoyed, or Bridge, even though they wanted to do something else. They got one apple. <br>
0 At some point in the fall, Emma did not play Dominion, which made them feel personally attacked over an issue of interpreting the rules since they have been playing this game for over ten years now, or Chess, which they though was a waste of their time. They got one apple. <br>
1 Perhaps in the spring and perhaps on the rainy day in the autumn when we were drinking cofee from a French hotel which we left theday after Haloween, Suzanne did not play Poker, which made them feel personally attacked over an issue of interpreting the rules since they have been playing this game for over ten years now, or Pool, while getting drunk. They got no apples. <br>
1 Once the sun rose, Masaya did not play Monopoly, which they though was a waste of their time, or Pool, which made them feel personally attacked over an issue of interpreting the rules since they have been playing this game for over ten years now. They got no apples. <br>
1 Having been buried, Olivia did not play Poker, even though they wanted to do something else, or Poker, while getting drunk. They got no apples. <br>
1 Perhaps in the spring and perhaps on the rainy day in the autumn when we were drinking cofee from a French hotel which we left theday after Haloween, Yi Ming did not play Monopoly, including murdering their own ghost in the cellar, or Cards, which they hated. They got no apples. <br>
0 In their last life, Taichi did not play Pictionary, regardless of age, or Checkers, regardless of age. They got one apple.<br>
0 Once the sun rose, Masaya did not play Roulette, regardless of age, or Checkers, regardless of age. They got one apple. <br>
1 Perhaps in the spring and perhaps on the rainy day in the autumn when we were drinking cofee from a French hotel which we left theday after Haloween, Olivia did not play Cards, including murdering their own ghost in the cellar, or Chess, even though they wanted to do something else. They got no apples.<br>
1 Once upon a time, Bai Qi did not play Scrabble, which they enjoyed, or Uno, regardless of age. They got no apples. <br>
1 Yesterday, Joseph did not play Go, which made them feel personally attacked over an issue of interpreting the rules since they have been playing this game for over ten years now, or Pictionary, which they though was a waste of their time. They got no apples. <br>
1 Yesterday, Miles did not play Scrabble, despite having to forget how to count, or Backgammon, which they though was a waste of their time. They got no apples.<br>
0 Once the sun rose, Bai Qi did not play Backgammon, even though they wanted to do something else, or Dominion, even though they wanted to do something else. They got one apple. <br>
1 Yesterday, Isaac did not play Roulette, regardless of age, or Blackjack, including murdering their own ghost in the cellar. They got no apples. <br>
0 In their last life, Shi Huang did not play Go, which they enjoyed, or Pictionary, which they hated. They got one apple. <br>
1 Perhaps in the spring and perhaps on the rainy day in the autumn when we were drinking cofee from a French hotel which we left theday after Haloween, Erin did not play Scrabble, regardless of age, or Smash, which made them feel personally attacked over an issue of interpreting the rules since they have been playing this game for over ten years now. They got no apples. <br>
1 At some point in the fall, Taichi did not play Cards, while getting drunk, or Pool, despite having to forget how to count. They got no apples. <br>
0 After they ate all the people, Masaya did not play Chess, even though they wanted to do something else, or Uno, regardless of age. They got one apple.<br>
0 At some point in the fall, Derek did not play Catan, even though they wanted to do something else, or Go, which they though was a waste of their time. They got one apple. <br>
0 In their last life, Taishu did not play Backgammon, despite having to forget how to count, or Checkers, regardless of age. They got one apple. <br>
<br>
</details><br>


### Outputs

>  These outputs are for the stimuli given above.

Here are the outputs for the three sets:

```python
set1 = "1 0 0 0 1 1 1 1 0 0 1 0 1 0 0 0 1 0 0 0 0 0 0 1 0 1 1 0 1 1"
set2 = "1 0 1 1 0 1 1 1 0 0 0 0 0 1 0 0 1 1 1 1 1 1 0 1 0 0 1 1 0 0"
set3 = "0 1 0 0 1 0 0 1 0 1 0 1  0 1 0 1 0 0 1 0 0 1 0 0 1 0 1 0 0 1"
```

And here's how these stimuli were expected to be evaluated (the nativelike values for an English speaker):

```python
# e for expected
set1e = "1 0 0 0 1 1 1 1 0 0 1 0 1 0 0 0 1 0 0 0 0 0 0 1 0 1 1 1 0 1"
set2e = "0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0"
set3e = "0 1 1 1 1 1 0 0 1 0 0 0 1 1 1 1 0 0 1 1 1 1 0 1 0 1 1 0 0 0"
```

We'll get to set 2 which is very intriguing very soon, but before that, let's take a closer look at sets 1 and 2. They seem to be quite similar. In fact, with a convenient

```python
from difflib import SequenceMatcher
def match(s1, s2):
    return SequenceMatcher(None, s1, s2).ratio()
```

we can determine the following ratios


| s  | sNe         | r    |
|:--|:--------------------------------|:------------------|
| s1 | s1e | 0.9661016949152542 |
| s2 | s2e | 0.4576271186440678 |
| s3 | s3e | 0.5210084033613446 |

Oops! In the most straightforward case, ChatGPT isn't too bad (but this was just 30 stimuli!), but when things get complicated, things get worse. For <code>set3/set3e</code>, it seems that complex input confuses ChatGPT and it is unable to compute the values correctly. It's about chance level at 52.1%.

However, <code>s2/s2e</code> is something entirely different. Let's revisit the instructions:

> Students were reading books. For each book they read, they received a rose. If the student read 0 books, they got 0 roses;
if the student read 1 book, they received 1 rose. For the list of the following scenarios, evaluate how many roses the student received.
Give your output as a string of 30 numbers, one for each scenario. Only output a string of numbers.

In this case, no students should have received a single flower! Since the sentences are of form <code>[X did not VP or VP]</code>, obviously nobody read anything in English judgments. The output seems at chance. Let's run it again with the same prompt and stimuli and see the result. We get[^8]:

[^8]: I am pretty sure that at least once the string was 28 characters long. I do not know why. Not me saying it, <code>print(len(s2e_2))</code> saying it.

```python
s2e2_0 = "011101010000110100100011101001"
s2e2_1 = "010110011000010001001100010110"
s2e2_2 = "010001000000010000010010010101"
s2e2_3 = "011001010010100001010010101011"
```  

The math ratios are:

| s  | respective of s2        | respective of s2e    |
|:--|:--------------------------------|:------------------|
| s2e2_0 | 0.29213483146067415 | 0.3595505617977528 |
| s2e2_1 | 0.3146067415730337 | 0.4044943820224719 |
| s2e2_2 | 0.29213483146067415 | 0.4943820224719101 |
| s2e2_3 | 0.29213483146067415 | 0.38202247191011235 |


### Is ChatGPT output _really_ random for Set Two?

It seems that way from the numbers just obtained, but let's verify this.

Let's adopt the following scenario, to see whether the behavior might change depending on how the scenario is stated:

> "For the following scenarios, determine how many books a student read in every scenario. Output a string of numbers, one for every scenario."

Then, let's run:

```python
s2_50 = prompt + set_two(50)
s2_100 = prompt + set_two(100)
s2_250 = prompt + set_two(250)
text = openai.ChatCompletion.create(
  model="gpt-3.5-turbo",
  messages=[{"role": "user", "content": s2_[n]}]
)
what_we_want = text.choices[0].message.content
print(what_we_want)
```

The outputs are quite puzzling. After running it a few times on everyone of 50,100, and 250[^9], the following were obtained.

> 010111010110111001101011100100100110101101101001010011011001010110100101011100101000100111010010101001101110100011010010101001

> 0111010000011100011111011010101001001100100001110000011010001101010

> 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
> 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

> 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 2, 2.

> Miles: 4, Erin: 4, Joseph: 4, Olivia: 2, Haeun: 5, Shi Huang: 3, Suzanne: 4 ,Keith: 4 ,Taishu: 4, Derek: 9 ,Wu Di: 3, Emma: 3, Masaya: 5, Yi Ming: 3, Davi: 3, Rob: 6, Bai Qi: 3, Isaac: 2, Taichi: 4, Tae In: 3, Karen: 2, Gregory: 4

> Keith: 2, Yi Ming: 2, Erin: 3, Taichi: 5, Miles: 4, Derek: 4, Isaac: 5, Suzanne: 4, Taishu: 3, Olivia: 4, Wu Di: 6, Karen: 4, Rob: 6, Shi Huang: 9Joseph: 4, Davi: 4, Bai Qi: 6, Tae In: 6, Gregory: 5,Haeun: 4, Emma: 7

What we are looking are zeros, as per English judgments. We get this in just one string -- but the length of that string does not match 50, 100, or 250. The behavior generally seems random, and for larger inputs, GPT seems to ignore the input and output total books for a student. Still, we can infer that judgements are not consistent with English (human) ones since the number of books is nonzero.    

[^9]: Larger output is not possible since this model caps input at ~5k tokens.


### Is ChatGPT's output for Set One really good?


So, in the table above, we remember that ChatGPT was pretty good at the most straightforward case of not-over-or. However, that was just one attempt. Let's try to replicate it.

The setup is as follows:

> Students were reading books. For each book read, they were given a flower. Evaluate the list of following scenarios and for each, determine whether it was possible in this setup. For each scenario, if it is possible, output 1, and if not -- 0. Only output a string of numbers. The scenarios are as follows:

So that we do not have to compute possible vs impossible scenarios by hand to compare ChatGPT's output to the expected output, here's a convenient function:

```python
def expected(s):
    l = s.split("\n")
    e = ""
    for i in l:
        if "no flowers" in i:
            e = e + "1"
        elif "one flower" in i:
            e = e + "0"
        else:
            e = e + "X"
    return e
```

To obtain a number of samples from which to draw some conclusions, let's adopt the following procedure:

```python
stimuli = set_one(10)
content = scenario + stimuli
text = openai.ChatCompletion.create(
  model="gpt-3.5-turbo",
  messages=[{"role": "user", "content": content}])
what_we_want = text.choices[0].message.content
# to see what's the output
print(what_we_want)
print(expected(stimuli))
# to compute the similarity ratio
ratio = f'{match(what_we_want, expected(stimuli))}\n'
# to save similarity ratios
with open('ratios.txt', 'a') as f:
    f.write(''.join(ratio))
```

Using this setup, we can obtain a large number of samples for batches of ten and twenty five scenarios. Having obtained these, we can visualize the distribution of ratios as follows:

```R
len10 <- ggplot(data = gpt1, mapping = aes(x = len10)) +
  geom_histogram(color = "white", fill = "steelblue", binwidth = .035)+
  geom_density(aes(y=.035*..count..))+
  labs(x = "Match ratios", y = "#of instances",
       title = "Distribution for match ratios of match(GPT output, expected output), n = 88; len10")
len10pic <- len10 +
  theme_gray(base_size = 100)
pngfile <- fs::path(knitr::fig_path(),  "theming.png")
agg_png(pngfile, width = (33), height = (12), units = "cm", res = 150)
plot(len10)
invisible(dev.off())
knitr::include_graphics(pngfile)
```

The histogram for batches of ten stimuli is:

![](https://dozernyi.gitlab.io/ling/docs/img/len10.png)


This suggests that the output is random, with normal distribution.

And for 25 is:

![](https://dozernyi.gitlab.io/ling/docs/img/len25.png)


This suggests a normal distribution with some influence (the second peak) -- this might be because ChatGPT is responding to the prompt in two ways (or,more precisely, gives two types of outputs), one of which is particularly bad at matching with the expected one.

### Is it because ChatGPT learns?

We see that the output is chaotic aka random, but does ChatGPT become better or worse with time as it gets more of the same input?

We can enumerate the ratios values we wrote to <code>ratios.txt</code> and the larger the number the later was the value written. With this in mind, we can see whether there's a correlation between number (representing a timeline) and ratios. Let's only do this for batches of ten since the result is likely to be the same for the batches of 25 stimuli.

We'll do both a scatterplot and a model:

```r
plot <- ggplot(gpt1, aes(x = num, y = len10)) +
  geom_point() +
  geom_smooth(method = "loess")
model <- lm(len10 ~ num, data = gpt1)
summary(model)
```

The plot is:

![](https://dozernyi.gitlab.io/ling/docs/img/plot.png)

And the results of the model are:

```
Residuals:
     Min       1Q   Median       3Q      Max
-0.34325 -0.08307 -0.00085  0.07880  0.24524

Coefficients:
              Estimate Std. Error t value Pr(>|t|)    
(Intercept)  0.5450335  0.0255197  21.357   <2e-16 ***
num         -0.0003649  0.0004980  -0.733    0.466    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 0.1187 on 86 degrees of freedom
Multiple R-squared:  0.006202,	Adjusted R-squared:  -0.005354
F-statistic: 0.5367 on 1 and 86 DF,  p-value: 0.4658
```

This is all to say (particularly the p-value of .47) that the timeline is **not** a reliable/accurate predictor match ratio. ChatGPT, then, is just bad at not-over-or generally, whether it gets a lot of input or not. It seems it does not learn/change responses over time.



## Input to ChatGPT: Ukrainian


First, let's see whether ChatGPT can deal with Ukrainian at all. To do so, let's ask it a simple question (via a familiar interface not to be repeated here; same as the one we used to ask about colorless green ideas):

> "Який сюжет твору Я (Романтика)?" [ENG: What is the piece "Ja (Romantyka)" about?]

The output is as follows:

> Я (Романтика) – це віршований твір, автором якого є український поет Андрій Малишко. Сюжет твору полягає в описі пейзажу, який створюється природою під час місячної ночі, та роздумах про природу людських почуттів і взаємовідносин у коханні. Поет описує свої відчуття і емоції, які відчуває в спокійну осінню ніч, коли на небі світяться зорі, а навколо спокій і тиша. Він згадує кохану особу, яку любить, і відчувається, що любов є основою життя і повинна бути збережена на все життя. Таким чином, сюжет твору "Я (Романтика)" сформульовано у формі відтворення краси природи сприйманої автором, а також у вираженні його власних емоцій і думок про кохання.

This is 100% a lie, but it understood the question. It says that _Ja (Romantyka)_ is a poem by Adrii Malyshko which depicts a moonlit night and the person who the author is in love with. In reality, though, Malyshko did not start writing until 1930, and _Ja (Romantyka)_ was published in 1924 by the writer by the name of Mykola Khvyliovyj. It's not a poem about love, it's a novella about a communist fanatic  killing his mother who was a nun as a part of tribunal on religious people. So ChatGPT has zero clue about Ukrainian literature. This already seems like a bad start.  

However, let's test Ukrainian next. To save time, we're just going to use the procedure we used in the last experiment. That is, we're going to collect ratios as we did with English data. We're not going to look for consistency in Set Two stimuli -- we'll just collect match ratios for Set One sentences. The scenario is a translation of the English one:

> Учні читали книжки. За кожну прочитану книгу, вони отримували квітку. Завдання таке: для кожного сценарію поданого нижче, треба визначити чи таке могло статись насправді. Для кожного з сценаріїв, якщо він можливий -- друкуйте 1, а якщо ні -- то друкуйте 0. Відмовідь має були лише рядок цифр. Сценарії такі:

What we get is still random behavior:

![](https://dozernyi.gitlab.io/ling/docs/img/plot_ukr.png)

## Conclusions

So, the broad conclusions (for English) are as follows:

### ChatGPT can, in general, evaluate ~v configurations with some reliability in the simplest context.
### ChatGPT sometimes evaluates ~v randomly where a consistent output is expected.
### Complex input such where coordinated:disjoined NPs are modified by RCs or APs or similar XPs present a challenge for ChatGPT and judgments obtained are inaccurate.

## However, this task is difficult and requires resolving complex long-distance dependencies. Prompt engineering and seeing whether shorter prompts help to get the right output might be an interesting element of a future project. Similarly, inputting sentences one by one might help as well.

<br>
<br>

* * *
<br>
To see other projects, go [here](https://dozernyi.gitlab.io/ling).
<br>
<br>
* * *
<br>
<br>
