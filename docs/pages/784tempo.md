---
layout: default
---

## On boundaries of tempo variance in Allegro giusto of Schubert’s Klaviersonate a-moll Opus post. 143 Deutsch-Verzeichnis 784  

# in progress

### Abstract

In this paper, we investigate 19 performances of the first movement of Schubert’s Klaviersonate a-moll Opus. post 134 Deutsch-Verzeichnis 784, viz. _Allegro giusto_. We chose to focus on this particular movement because of two reasons: (a) its tempo marking, which dictates consistency, viz. Giusto, and (b) to increase the granularity of our analysis and be able to to track variance in tempo on phrasal level. After contextualizing the sonata and the relevant movement specifically, we proceed to partition the movement into twenty structural parts and manually record and calculate the tempo from time intervals between beats. We analyze the patterns which emerge across the performances and correlations of changes in tempo with Schubert’s dynamic markings. We also collect objective loudness measures (LUFS) for each recording and correlate the degree of variance in tempo with degree of variance in loudness/dynamics for each performance. We find strong correlations between dynamic variance and tempo variance, as well as strong correlation between changes in dynamics and changes in tempo almost universally across the performances we analyze. This study takes a novel, more nuanced approach to performance analysis and centers around composition, the composer, and the performer as opposed to audience and psychology of music perception – which has been the main trend in performance analysis studies heretofore. We hope that outcomes of this study will be able to both inform performance practices and encourage further nuanced investigations into performance practices.

### §1 Introduction

Schubert’s sonatas are situated in a liminal moment of shift from rigidity of Mozart- and Haydn-type "traditional" sonata form to relative freedom of Romanticism, both in terms of harmony, and in terms of form sensu _Formenlehre_. Amongst Schubert’s sonatas, much like amongst Beethoven’s, there’s a rather clear arc of development. The early ones show more adherence to "traditional" form whereas later ones show salient departure from it manifesting, e.g., in four-movement structure (e.g., D959 – likely (broadly) Beethoven’s influence[^1]), increasingly extended length, bold modulations, &c. – tendencies shared by developing forms of sonata and concerto alike around the time[^2]. One example that is perhaps telling of Schubert being situated in-between significant shifts of sonata form are the expositions: Beethoven’s expositions are repeated more or less dutifully (a less dutiful example would be his Es-dur concerto) whereas in Brahm’s sonatas, there is barely any recapitulation at all – similar logic applies to Schuman’s fis-moll sonata[^3]. Schubert stands in-between these.

One sonata stands out, situated at the liminal moment of Schubert’s own sonata oeuvre[^4] – and at the liminal moment of his life, namely his a-moll sonata written in 1823; Deutsch 784, Op. post 143[^5]. It is an incredibly grim piece, consisting of three movements: _Allegro giusto_, _Andante_, and _Allegro vivace_. A salient feature of D784 is its (repeated) stark dynamic contrasts – e.g., _pp_ to _ff_ and back to pp within three measures (e.g., mm 90-92). We note t]hat this paper focuses solely on _Allegro giusto_.

Performance history of the piece is strikingly diverse; suffice it to note that the duration for D784ag ranges from slightly over 8 minutes to 14 minutes. Flexible as allegro is, the difference is still rather stark. Dynamics seem similarly diverse, but cannot be adjudicated objectively, unlike tempo. We venture to quantitatively investigate both. Overall, then, the research questions of this paper are:

> (RQ1) how “allegro” are the performances of Op. post 143; viz. what is the extent of tempo variation across a representative sample of performances?

> (RQ2) how “giusto” are those?; viz. what is the extent of tempo variation within any given performance?, and

> (RQ3) is there a correlation between the range of dynamics[^6] within any given performance, and range of tempo variation[^7].

It is important to note that while absolute measures of loudness are not very reliable and differ depending on the recording, etc. – it is the relative range (LUFSmax - LUFSmin; see footnote 2) that we are interested in as opposed to absolute range.

We will proceed by splitting D784ag into several structural parts[^8], to increase the granularity of our analysis. Then, we analyze thirty-five available performances (the full list is given in Appendix A) recording meter, separately for every structural part we defined. These data, with some straightforward mathematical manipulation, then allow us to create plots of time in seconds against tempo in bpm at any given point in the performance. We later discuss the extent of absolute range[^9] of tempo variation generally as well as per structural unit of D784ag. We also use Insight software by IZotope to create a loudness contour for the performances we selected for analysis and determine the relative range of dynamics (_Δd_), relying on measures of momentary loudness. Proper tempo (bpm) determination is notoriously hard for software even just for percussion, so we created tempo curves for thirty-five performances manually, with a digital stopwatch. Lastly, to determine whether there is correlation between range of tempo (_Δv_)[^10] and range of dynamics, we run regression analysis. All visualizations and graphs in the paper, as well as statistical analyses, were generated with R (R Core Team 2022), using <code>tidyverse</code> and <code>dplyr</code> packages (Wickham et al. 2019).

The paper proceeds as follows: §2 contextualizes D784 and D784ag specifically within (a) Schubert’s life at the time, (b) Schubert’s work, and (c) contemporary music tradition – this section also foreshadows our analysis of structure; §3 discusses the structure of D784ag and suggests partition into structural units for empirical analysis; §4 briefly overviews the materials used in the study, and the methods used to collect measurements; §5 presents the results; §6 discusses the results with respect to the research questions and speculates on potential implications of these results, as well as on their generalizability; §7 concludes.

Lastly, it is critical to clearly demarcate the scope of this paper. In a relatively young field of performance analysis, a significant portion of existing studies have been devoted to discussing, often exclusively so, _perception_ of tempo, pitch, and other performance parameters. These studies are comprehensively overviewed in Lerch et al. (2020), and most of them have appeared in journals like _Musica Scientia_, _Frontiers in Psychology_, &c., i.e. journals that are primarily concerned with psychology of music perception. Yet, it seems to us that there is another side to performance analysis. If Schubert directs us to _Allegro giusto_ but certain performances show no sign of either (admittedly flexible) _allegro_ or (seemingly uncontroversial interpretation-wise) _giusto_  – then it might be useful to critically approach these performances from a somewhat novel, to some extent quantitative side, prompting the discussion how these performances may conflict with the composer’s intention and, more importantly, the context in which the piece was written. It is these desiderata that comprise the foci of this paper’s lens.

### §2 Contextualizing D784

D784 appeared at a very grim time in Schubert’s life. It was written in the winter of 1823, and the composer has been ill since at least December 1822, as attested in the letters. Specifically, in February he rejected an invitation writing that he would be unable to leave his home because of the illness. This points to a rather serious medical issue – quite possibly the onset of the widely hypothesized syphilis, if syphilis it was at the end. Either way, Schubert is likely depressed as soon after D784, he writes a short poem, "Mein Gebet" (deu. _My Prayer_) of ever so slightly suicidal nature[^11]. These circumstances are described in some detail in Chapter 2 of Gingerich (2014), to which the interested reader is directed[^12]. We stop here in terms of context of Schubert’s life since it has a somewhat limited bearing on the RQ of the paper.

Hatten (2016) found similarities between D784ag and the first act of Fierrabras, which is not unexpected since they were written in the same year. He points to intertextual connections and expressing particular emotions with (a) plagals from the opening of D784ag and _Un poco piu moto_ of _Fierrabas_, and (b) falling thirds, which seem to be appearing as fourths at least sometimes (d-g) in D784ag and dotted descend in finale of Act I of _Fierrabras_. For further details, see Hatten (2016:98-99).

Porter (1980) points to particular importance of major third of the dominant in the A-dur sonata D959, arguing that it was the third that became upper note in the reprise of the plagals (see unit Ab’ in the Table 1 below) as opposed to the previous tonic in the sonata that gave the phrase a "particularly striking" feel. It seems, though, that moving the plagal cadences up two octaves, turning a triad into a tetrachord, and (arguably)[^13] amplifying the dynamics seems a good enough reason for the "striking" character.

Overall, however, the available literature on D784 is extremely sparse: save Whaples (1984)[^14] and Damschroder and Hatten (2010), it is always mentioned only in passing and often as a contrast to other sonatas whereas independent analyses remain absent. It is telling that even in Költzsch’s monograph _Franz Schubert in seinen Klaviersonaten_, to which we return below, D784 is not given thorough analysis – even this singular monograph on sonatas alone singles out Op. 42 (D845) and mentions D784 in passing.

Lack of research pointed out above forces us to go further back, to Költzsch (1927), to see that he pointed out (rather vague) similarities between Beethoven’s A-dur cello sonata (Op. 69) and all a-moll D784, D845, and C-dur D840 (Költzsch 1927: 124). He thus calls into question arguments that D784 symbolized a new turn in Schubert’s sonatas and a departure from earlier form which, keeping to Beethoven, are seen as conservative against D958-960 and D784. Költzsch also points to somber circumstances of Schubert’s life emphasizing that it is perhaps telling that D784 stands out as a first solo piano work after a number of orchestral or chamber ones (Költzsch 1927:44).  

So, we already see how on the one hand, writing a movement (a) with somewhat unusual modulations, (b) consisting mostly of octaves (at least a quarter of the movement), Schubert departs from Classical and moves into Romantic. This liminality has been pointed out before (e.g., Newbould 1997). Then we have Költzsch (1927) pointing to similarities with Beethoven.-^15=

Lastly, to perhaps complete the picture of inconsistency, Krause (2015) writes that Schubert actually goes back to the baroque period in his use of _figura suspirans_, prominent in Monteverdi, Buxtehude, Bach, &c. (see Williams 1979 for an overview of _suspirans_ from a _Formenlehre_ perspective). Krause points to various implementations of _f.s._ in D784, and D784ag specifically; these "verschiedenen Metren der Suspiratio-Figur" are given in Krause (2015:412f).

Tempo is another issue that is rather divisive. For example, Hoffman clearly recommends “um ¼ = 132” for C-Takt (Hoffman 2001:124). He points out that Schubert was rather meticulous with tempos and had very diverse tempo markings (cf. Hoffman 2001: 128-132). However, Hoffman also points out that the two works with the tempo _Allegro giusto_[^16], namely D784ag and D597, _Overture D-dur in Italian style_, are strikingly different in structure and complexion. This complicates determining what allegro giusto is supposed to be in performance. Hoffman makes parallels between closing passages of the overture and the dotted rhythms of D784ag, but such parallels do not seem quite sufficient to base tempo for the entire piece on them – particularly given that D597 and D784 are separated by a not insignificant time interval (Hoffman 2001:103f).

Further, Krause (1991) – in what seems to be one of the two existing monographs on Schubert sonatas (alongside Költzsch) – points out that D784 was the last three-movement sonata of Schubert’s. He also elaborates quite extensively on its structure and tempo. For example, Krause suggests that _allegro giusto_ is, in fact, ineffective[^17] in the exposition and recapitulation, but it clearly determines the development (“deutlich bestimmt”; Krause 1991:30). No explanations are given as to why Schubert’s own tempo indication is taken to be ineffective for 70% of the movement (61 measures out of 292), so we disregard this suggestion.   

So, we arrive at D784, and D784ag specifically, being at the same time all the listed below:

* a step towards romanticism
* with influence of classical (early-middle-period) Beethoven
* and metrical roots in Bach and older Baroque,

which all makes for an argument that there is no conclusive analysis of what D784(ag) is. The existing analyses pointing in dramatically different directions evidentiate amazing versatility of the singular movement (in fact, the three below apply to the first page of the movement specifically) on a light take, or perhaps lack of coherent picture of etiology or structure of D784(ag) on a more grim reading. All the more reasons to look into performance practices.

### §3 Structure of Allegro giusto

The structure D784ag, quite clearly, consists of three main structural units: [A𝄇BA’] which extends to [AABA’], following a rather conventional sonata allegro form. (Some performances we will analyze omit A<sub>2</sub> in [A<sub>1</sub>A<sub>2</sub>BA’].) We then split the movement into 3 distinct parts. Further, it seems that to increase granularity of the analysis and be able to single out particular (some but not all) phrases for comparative analyses, we might benefit from splitting the [AABA’] further. One possible partition is given in the table below and we briefly discuss it immediately thereafter.

| # | Unit | Measures| # of measures|
|:--|:-----|:--------||:------------|
| 1| Aa| 1-8| 8|
|2|Ab| 9-21|13|
|3|Ac| 22-33|13|
|4|Ab'|34-46|13|
|5|Ad| 74-60|14|
|6|Ae|61-103|43|
|:--|:-----|:--------||:------------|
|7|Ba|104-109|6|
|8|Bb| 110-126|17|
|9|Bc| 127-136|10|
|10|Bd|137-160|24|
|11|Be|161-165| 5|
|:--|:-----|:--------||:------------|
|12|A'a|166-173|8|
|13|A'b|174-186|13|
|14|A'c|187-198|12|
|15|A'b'|199-210|12|
|16|A'd|260-276|17|
|17|A'e|220-259|40|
|18|A'g|277-292|16|
|19|A'h|211-219|9|



The partition is pretty much entirely thematic as opposed to harmonic.[^19] Note the parallels between Ab and Ab’, as well as Ab, Ab’, A’b, A’b’. They are almost identical in terms of size, only A’b’ is slightly truncated. A’g and A’h are taken out of the main A’ group sequence since they are additions which were not in A. Similarly, A’ is somewhat restructured, i.e. the order of the score is A’b’– A’h – A’e – A’d – A’g, but the labels were kept to maintain the thematic correspondence between A and A’.

#### Group A

The Aa unit is the main theme, repeated throughout the movement; this seems a sufficient reason to single it out in order to be able to clearly see variance in how the theme itself is performed both as Aa and as A’a (i.a., e.g. mm. 98-99 and 101-102)[^20]. Similarly independent is the next unit of Ab which consists mainly of plagal cadences in the left hand, IV to I, i.e. subdominant d to a. Though, they are not exclusively clear plagal cadences alternating dominant-tonic and mediant-tonic. These cadences will appear again two octaves higher in Ab’. Again, the group of cadences which is repeated a number of times throughout the movement warrants analyzing them as a separate structural unit. In Ac we obtain what is one of the first stark dynamic contrasts, in two sequences of (mainly) falling fourths. Ad serves as the transition – again, apple with sharp contrasts, e.g. PP transition from repeated Ab’ to accelerating octaves at FF which alternate with whole notes in mm. 55-56 (and the same in 59-60) at P in sharp contrast to FF. Ad leads us to transition to E major, which will remain for a relatively short time, and even within this short period, it shall be dynamically subservient (ff-p) to minor.[^21] This concluding part is Ae.

#### Group B

The development starts with oscillating fifths in the left in Ba with ascending half-notes in the right. Bb is perhaps the climax with falling octaves in dotted rhythm in the left following the pattern of Ac. Then, perhaps similarly to the transition Ac-Ad-Ae, the key moves back and forth from subdominant and submediant, but we do not observe sharp dynamic contrasts which accompanied similar changes in the key in Ae. Very soft transitions, like the one in m. 145, are in a sharp contrast to Group A. Bb-Bc-Bd comprise a more or less clear arc of moving from ff to pp, returning to the subject of Aa in Ae at pp.

#### Group A’

This group expectedly follows A in a straightforward parallelism seen in Table 1. The only two departures are a slight truncation and therefore shift of A’b’ and A’e and transitions added perhaps to accommodate rearrangement of subjects from Group A, namely A’g and A’h. Sharp dynamic contrasts return, effectively abandoning the flow established in Group B.


### §4 Data, materials, and methods

The recordings we used for analyses were 19 recordings of D784ag. They are given here in alphabetical order: Ashkenazy, 1995; Badura-Skoda, 1971; Barenboim, 2014; Bolet, 1990; Brendel, 1984; Bruno, 2019; Debargue, 2017; Jandó, 1993; Kempff, 1968; Kissin, 1995; Kraus, 1937; Leonsjaka, 2016; Pires, 1989; Schiff, 2011; Sohn, 2020; Sokolov, 2014; Uchida, 2000; Youn, 2020.

The tempo measurements were recorded manually, with granularity of two beats per measure.[^22] In our calculations we used beat-measure correspondence to single out units as given in Table 1. The loudness measurements were done with iZotope software, of which we used momentary loudness (as opposed to, e.g. short term). The tables with time (ms) – momentary loudness (LUFS) correspondences served as raw data. As mentioned, all of the analyses and visualizations were done in R (R Core Team 2022).

The momentary tempo, i.e. tempo at any given beat, was calculated as 60/bn_1-b_n, i.e, we calculated the time between the last beat and the beat in question, and how many of these time intervals would fit in a minute. Obtaining a cumulative value averaging all previous time intervals would not accurately portray changes in tempo, so this is the preferred means from the granularity perspective. Also, the number of beats is set, with two per measure they amount to 585 per recordings. We, then, measured the total of 20 475 beats for the purposes of this study. Such division also aids cross-recording comparison since the only thing in common for these recordings are the number of beats.

Further, there were recordings differing in the ordering of the section, as mentioned elsewhere in the paper: some played AABA’ and some played ABAA’. We nivellated these differences by adopting one unified partition (as given in Table 1), and rearranging the recorded numbers so that they fit that partition, e.g. by swapping second A and B sections. We do not see this as a confounding variable for our analysis since we do not ask about the patterns in the overall, hypersectional tempo contour – rather about the range and consistency of tempo which is a more local parameter.

§5 Results

§5.1. Tempo measurements

The faceted graph on the next page gives a general impression of the data we obtained.


![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/overall.png)
Figure 1. Summative tempo contours faceted by performer
(breaks indicate that the performer omitted repetition of the exposition)

![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/overall2.png)
Figure 2. Lines of best fit (lm/polynomial) for tempo contrours (the breaks indicate omitting a movement)


As we can see in Figure 1, there is a great deal of variance and it is virtually impossible to distinguish the overall trend from the data. However, if we look to Figure 2, we see a somewhat clearer pattern, due to the trendlines. Let us, however, move to consider each structural part of the D784ag separately. We start with Group A.

§5.1.1. Group A

Figure 3 below gives lines of best fit for Group A (A1, i.e. without the repetition of A which some performers omit) for all performers. In Group A, we have six units (Aa, Ab, Ab’, Ac, Ad, and Ae). We consider them in turn. (We also now change x-axis from Beat to Measure, i.e. Beat/2 for convenience.)

Aa
![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/Aa.png)

Figure 3. Tempo contour for Unit Aa (polynomial regression, Tempo vs. Measure)

Most of the performers start relatively high, from around 120 to above 200 – the distance between the half notes which open the movement is shortened. Most then go down, unable to support such speed with fourths and sixteenths. Relative consensus is reached somewhere around the middle, viz. measure 4-6. After that, with the syncopation at the end of measure 6 and relative climax in measure 7, there is a sudden increase in tempo for most, which falls down again with the descent in m. 8 preparing for plagal cadences to come in in Ab.

Ab
![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/Ab.png)


Figure 4. Tempo contour for Unit Ab (polynomial regression, Tempo vs. Measure)

Somehow, the plagal cadences stabilize most of the performers (it seems Sokolov is a notable exception who goes above most others at 125bpm). So, measures 10 through around 17 which is mostly plagal cadences in the left and right before crescendo in the right show a relatively stable pattern, whereas approaching measure 20 which augurs the upcoming falling thirds, the tempo converges for between 100-150 for most, ready to lead (and for some already leaping, as can be seen at the edge of the graph – in Richter’s case, up to 200bpm).

Ac
![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/Ac.png)

Figure 5. Tempo contour for Unit Ac (polynomial regression, Tempo vs. Measure)

First, note how uniformly everyone is above 100 contrasting the previous picture. We also see, with remarkable clarity, all performers pretty much without exception slowing down in measure 25, spreading up at about 27 and slowing down at 30 again. Indeed, the falling thirds are preceded with half notes which are in measures 26 and 30, explaining this effect. Contrasting the eighth-sixteenth with half-notes makes for such unity.


Ab’
![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/Ab'.png)

Figure 6. Tempo contour for Unit Ab’ (polynomial regression, Tempo vs. Measure)

Ab’ is the repetition of the plagal cadences, but this time they come in ff as opposed to pp-f. Somehow, the repetition happens also at a tempo that is uniformly faster than that of Ab. For the cadences that are two octaves higher and became tetrachords, the tempo is maintained above 100 and moves down – for some but not for all – only at the descend into pp in mm. 44-49. There also seems to be a tendency to increase the tempo before measure 48, which is not straightforwardly explicable from the score. Since not only the dynamics, but the entire measures 47-50 are entirely identical.

Ad
![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/Ad.png)

Figure 7. Tempo contour for Unit Ad (polynomial regression, Tempo vs. Measure)

Ad is the section which arguably encompasses the climax of Group A. Starting with pp-f contrast in measures 50-51, with the cresc. given by Schubert in measure 51, we see a united increase in tempo, going hand in hand with the crescendo. Measures 55-56 are at p, before ff again in 57 and then pp again in 59-60. These correlate perfectly with tempo: 55-56 the dip before a rather sharp increase in 57-58 and an even sharper dip in 59-60. Oddly, while from p in 60, Schubert moves to pp in 61 which stays until ff in m. 79, there seems to be an increase right at 62, which is the beginning of Ae, the last and biggest (mm. 63-103) unit of Group A, full of dynamic contrasts.

Ae
![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/Ae.png)
Figure 8. Tempo contour for Unit Ae (polynomial regression, Tempo vs. Measure)

 In this case, unlike in the previous section, stark contrasts in dynamics do not seem to have any significant effect on tempo, e.g. ff-p in mm.79-80, 83-84, ff-pp in mm. 86-87, 91-92. This is somewhat at odds with the previous tendencies, as mentioned. Otherwise, the tempo seems to stay remarkably stable for this concluding section. This could perhaps be attributed to the unchanging rhythmical pattern throughout the section: two half notes followed by four fourths.

§5.1.2. A1 vs. A2

For those performers who did have the repetition of A1 in the form of A2, we now give a faceted by Group (A1 vs A2) overall graph. The intention here is to see consistency between tempo patterns in A1 and A2.

![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/A1vsA2.png)
Figure 9. Tempo contour comparison for Group A1 vs Group A2
(polynomial regression, Tempo vs. Measure)
Indeed, the similarity is quite clear. Increase in tempo around 50-75 is the same, as is the increase at 100. The dips before 50 and right after 75 are also not dissimilar. Overall, there is commendable consistency in how A1 vs A2 tempo patterns coincide.

§5.1.3. Group B

Overall, Group B is very stable, as the following figure attests:

![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/B.png)
Figure 10. Overall tempo contour for Group B (polynomial regression, Tempo vs. Measure)

We shall, however, briefly consider the units below.

… to be completed …

![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/B.png)
![](https://dozernyi.gitlab.io/ling/docs/img/74tempo/Ba.png)
![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/Bb.png)
![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/Bc.png)
![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/Bd.png)
![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/Be.png)

§5.1.4. Group A’


A’a

![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/A'a.png)

A’b

![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/A;b.png)

A’c

![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/A'c.png)

A’b’


A’d

A’e

A’f

A’g

A’h
![](https://dozernyi.gitlab.io/ling/docs/img/784tempo/A'h.png)

§5.1.5. A vs. A’


§5.2. Loudness analyses

§5.3. Loudness-tempo correlation analyses


§6 Discussion

Our research questions were as follows (restated here for convenience):

(RQ1) how “allegro” are the performances of Op. post 143; viz. what is the extent of tempo variation across a representative sample of performances?
(RQ2) how “giusto” are those ?; viz. what is the extent of tempo variation within any given performance?, and
(RQ3) is there a correlation between the range of dynamics within any given performance, and range of tempo variation.

With respect to (RQ1):

Abc

§7 Conclusive remarks


…


Reveal new systematicities in performance, hitherto unknown – on a new, pretty much phrasal, level of granularity. The scale – i.e. one movement, almost phrasal focus – does not mean the results are not generalizable or somehow uninformative.



## References
</br>

Ashkenazy, V. (1995). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Decca.<br>
Badura-Skoda, P. (1971). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Sony Music Entertainment.<br>
Barenboim, D. (2014). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Deutsche Grammophon.<br>
Bodley, L. B. (2016). In Pursuit of a Single Flame? On Schubert’s Settings of Goethe’s Poems. Nineteenth-Century Music Review, 13(1), 11–33. https://doi.org/10.1017/S147940981500049X<br>
Bolet, J. (1990). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Decca.<br>
Brendel, A. (1984). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Decca.<br>
Brendel, A. (1988). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Decca.<br>
Brendel, A. "Schubert's Last Three Piano Sonatas." RSA Journal 137.5395 (1989): 401-411.<br>
Brewerton, Erik. "After Playing Schubert's Pianoforte Sonatas." The Musical Times 69.1028 (1928): 887-889.<br>
Bruno, G. (2019). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. OnClassical.<br>
Byrne Bodley, L., & Horton, J. (Eds.). (2016). Schubert’s Late Music: History, Theory, Style. Cambridge University Press. https://doi.org/10.1017/CBO9781316275887 <br>
Cooper, I. (1987). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Avie Records.<br>
Damschroder, D., & Hatten, R. S. (2010). Piano Sonata in A Minor (D. 784), movement 2. In Harmony in Schubert (pp. 171–190). Cambridge University Press.<br>
Debargue, L. (2017). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Sony Classical.<br>
Douglas, B. (2020). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Chandos.<br>
Faistauer, R. A. (2022). In Search of Clear Execution: An Eighteenth-Century Approach to Franz Schubert’s Piano Sonata in A Major D. 959 (Doctoral dissertation, Northwestern University).<br>
Feltsman, V. (2017). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Nimbus Alliance.<br>
Gingerich, J. M. (2014). Schubert’s Beethoven Project. Cambridge University Press. https://doi.org/10.1017/CBO9781139032193 <br>
Hatten, R. S. (2016). Schubert’s alchemy: Transformative surfaces, transfiguring depths. In J. Horton & L. Byrne Bodley (Eds.), Schubert’s Late Music: History, Theory, Style (pp. 91–110). Cambridge University Press. https://doi.org/10.1017/CBO9781316275887.006<br>
Hoffman, Helmut. (2001). Franz Schubert: Das Zeitmaß in seinem Klaviermusik. Musk-Konzepte 114;X.  ISBN 3883776734. Richard Boorgerg Verlag Gmbh & Co.<br>
Jandó, J. (1993). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Naxos.<br>
Kempff, W. (1968). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Deutsche Grammophon.<br>
Kissin, E. (1995). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Sony Classical.<br>
Klien, W. (1996). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Vox Box.<br>
Költzsch, H. (1927). Franz Schubert in seinen Klaviersonaten. Breitkopf & Hartel.<br>
Kraus, L. (1937). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. INA.<br>
Krause, A. (1991) Die Klaviersonaten Franz Schuberts: Form - Gattung - Ästhetik. Bärenreiter. ISBN 9783761810460<br>
Krause, A. (2015). Die Klaviermusic. In W. Dürr & A. Krause (Eds.), Schubert Handbuch (4th ed.). Bärenreiter.<br>
Leonsjaka, E. (2016). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Warner Classics.<br>
Lerch, A., Arthur, C., Pati, A., & Gururani, S. (2020). An Interdisciplinary Review of Music Performance Analysis. Transactions of the International Society for Music Information Retrieval, 3(1), 221–245. https://doi.org/10.5334/tismir.53<br>
Lupu, R. (1971). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Decca.<br>
Maxwell, Carolyn, Eileen Cline, and Geraldine Gant Luethi, eds. Schubert, Solo Piano Literature. Maxwell Music Evaluation, 1986.<br>
Mak, Su Yin Susanna. Structure, design, and rhetoric: Schubert's lyricism reconsidered. University of Rochester, Eastman School of Music, 2004.<br>
Maisenberg, O. (1983). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Orfeo.<br>
Margolina, E. (2021). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Ars Produktion.<br>
Mies, P. (Ed.). (1961). Klaviersonate a-moll Opus post. 143 D 784. G. Henle Verlag.<br>
Newbould, B. (1997). Schubert: The Music and the Man. University of California Press.<br>
Perotta, M. (2017). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Decca.<br>
Pienaar, D.-B. (2020). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Avie Records.<br>
Pires, M. J. (1989). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Deutsche Grammophon.<br>
Planes, A. (1993). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Harmonia Mundi.<br>
Porter, E. G. (1980). Schubert’s piano works. Dobson.<br>
Reichert, A. (1997). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Harmonia Mundi.<br>
Richter-Haaser, H. (2013). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Warner Classics.<br>
Rosel, P. (2000). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Capriccio.<br>
Sams, E. (1980). Schubert’s Illness Re-Examined. The Musical Times, 121(1643), 15–22. https://doi.org/10.2307/963189<br>
Schiff, A. (2011). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Decca.<br>
Schuchter, G. (2017). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Tudor.<br>
Sohn, J. B. (2020). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Genuin.<br>
Sokolov, G. (2014). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. JSC “Firma Melodiya.”<br>
Uchida, M. (2000). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Decca.<br>
Whaples, M. K. (1984). Mahler and Schubert’s A Minor Sonata D. 784. Music & Letters, 65(3), 255–263.<br>
Wickham, H., Averick, M., Bryan, J., Chang, W., McGowan, L. D., François, R., Grolemund, G., Hayes, A., Henry, L., Hester, J., Kuhn, M., Pedersen, T. L., Miller, E., Bache, S. M., Müller, K., Ooms, J., Robinson, D., Seidel, D. P., Spinu, V., … Yutani, H. (2019). Welcome to the tidyverse. Journal of Open Source Software, 4(43), 1686. https://doi.org/10.21105/joss.01686<br>
Williams, L. (2020). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Signum Classics.<br>
Williams, P. (1979). Figurenlehre from Monteverdi to Wagner. 3: The Suspirans. The Musical Times, 120(1638), 648–650. https://doi.org/10.2307/962468<br>
Youn, W. (2020). Piano Sonata No. 14 in A Minor, Op. 143, D. 784: I. Allegro giusto. Sony Music Entertainment.<br>

</br>
</br>

[^1]: However, for some considerations on Schubert’s departures from Beethoven, see Mak (2004) and Brendel (1989). For a recent comprehensive treatment of D959 specifically and its performative etiology, see Faistauer (2022).

[^2]: See Byrne, Bodley and Horton (2016) for a more extensive discussion of these patterns in Schubert’s work in late life.

[^3]: If we consider Un poco adagio (52 measures) to be exposition, which is controversial, then “recapitulation” of that exposition is limited to eleven bars in the left hand of Allegro vivace (mm. 268-278). If, on the other hand, we take the first (A𝄇) fragment of Allegro vivace to be the exposition, it will not be any less problematic since the main theme is repeated close to every five bars on average in all shapes or forms possible which makes it a hard but noble mission to find clear development and recapitulation.

[^4]: Krause remarks so much, noting that D784 started “die zweite Phase” in his sonatas (Krause 1991:13). One of possible arguments is D784’s overall defeasance of “fast-slow-fast” form (Krause 1991:31).

[^5]: We’ll refer to the target score, which is the first movement of D784, as D784ag (for Allegro giusto) from here on in the paper. Same goes for occasional mention of D784a (Andante) and D784av (Allegro vivace).

[^6]: It should be noted that dynamics is used ambiguously here. By dynamics, we mean either Schubert’s own directions in the score – or, mainly when we discuss “range of dynamics”, we refer to numeric measurements of LUFS (Loudness, K-weighted, relative to full scale). The context will clearly distinguish the two.

[^7]: Straightforwardly, by range of tempo variation we mean the difference between the fastest and the slowest tempo recorded for a given performance: with more stark contrasts, range is bigger. We will return to these definitions briefly immediately below and discuss this at some length in the methods section.

[^8]: We note that the score used for this paper was HN 623 of G. Henle Verlag.

[^9]: It is “absolute” in the sense that we are comparing every performance against an objective measure of time – bpm, as opposed to just looking at the range. In the latter case, the performances that show ranges {90, 110} and {115,135} would be treated the same since the measurement is relative, not absolute. We want to avoid this with tempo, but we want to keep this with respect to dynamics – since “loudness” is not an absolute measure whereas bpm is.

[^10]: We use Δv to refer to the range of tempo instead of Δt to avoid confusion with time which we do notate as Δt.

[^11]: For a brief analysis of the poem and larger motifs of death in Schubert, see Bodley (2016).

[^12]: For further discussion, see references in Gingerich (2014:41-47), but particularly Sams (1980).

[^13]: The ff marking on Ab’ is disputed by the editors of Henle G. Verlag Urtext edition, pointing out that more plausible is the parallel with Ab which was rising from p to f. See Mies (1961/1989:19).

[^14]: It is also worth noting that Whaples (1984) focused perhaps more on the (putative) influence of D784 on Mahler’s life as opposed to on sonata itself. Nonetheless, the paper principally engages this sonata, and so we cite it.

[^15]: Notably not late Beethoven, "early middle Beethoven" – A-dur cello sonata is dated 1808-09.

[^16]: Hoffman also points out that for Czerny, _guisto_ meant “mit Maß”, i.e. “with measure” (103).


[^17]: The exact phrasing is “nur seltem wikrsam”, i.e. only rarely effective.

[^18]: It is perhaps worth pointing out that there are some early 20th-century writings in the spirit of Mahler which wrote of D784: “...the limitations of Schubert’s pianistic accomplishment and the rapidity of his writing are responsible for a monotony of figuration and lack of grace. Some passages [pointing here to mm. 61-56 of D784ag – author] hardly look like original pianoforte music, but resemble a reduction from another medium…” (Brewerton 1928). We point it out as a striking oddity as opposed to a point worth entertaining.

[^19]:The modulations in this movement – while unusual – are quite sparse as compared to earlier sonatas; this is another salient departure from customary convention of Schubert’s. Another reason not to guide partition by harmony, &c. directly are the conflicting analyses briefly overviewed in the previous section. Still, worth pointing out that Krause writes about “tonal subtlety” as the main reason behind creation of D784, breaking Schubert’s “period of crisis” (Krause 1991:153). To our mind, it is perhaps more the life circumstances that dictated the necessity for expressing complexity for which tonal subtlety was requisite – as opposed to it being the other way around on Krause’s suggestion.

[^20]: Some (like Newbould 1999) note that the theme/subject is quite similar to that of D625, the f-moll sonata, written in 1818, some five years before D784 and never completed. There are, however, plenty of dotted and half notes elsewhere in Schubert, and so the connection seems somewhat farfetched. At the same time, Maxwell et al. agrees, writing that “the dotted eighth/sixteenth idea becomes the tool of the most dramatic intensity” (Maxwell et al. 1986:286).

[^21]: Again, in contrast to Newbould who sees this as a “firm” establishment of E major. But it vanishes within ten bars, with occasional and infrequent return, exclusively in PP in contrast to A minor returning in FF at mm. 79, 83, &c.

[^22]: Also, our choice was partly motivated by Krause (1991:30) adjudicating that the movement is actually in “2/2-Takt”, as the first measures of D784ag suggest. The other reason which dictated that we record two as opposed to four beats per measure was purely logistical – as it is, we recorded 15 030 time measurements manually whereas 30 060 would have been significantly less feasible – and also equally unnecessary.

[back](https://dozernyi.gitlab.io/lsalatex)
