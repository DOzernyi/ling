---
layout: default
---

## **Projects maintained by D. M. Ozernyi:**

|   | Page          | About    |
|:--|:--------------------------------|:------------------|
| 1 | [LSA & LaTeX](https://dozernyi.gitlab.io/lsalatex)                | Guidelines for using LaTeX templates for publications of the Linguistic Society of America|
| 2 | [ChatGPT and disjunction under negation](./docs/pages/disj.html)| Does ChatGPT render authentic judgments for disjunction-under-negation configurations?|
| 3 | [How transfer works](./docs/pages/transfer.html)| Explanation of models of transfer in L2/Ln acquisition |
| 4 | [Tempo in D. 784 I. _Allegro giusto_](./docs/pages/784tempo.html) | On boundaries of tempo variance of performances of Schubert's *a-moll* sonata D. 784 |

> With questions, reach out to me at [dozernyi.com](https://dozernyi.com) or *doz at u dot northwestern edu*.
